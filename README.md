# README #

The files found here are a result of following the [Web Application Architectures MOOC on Coursera](https://class.coursera.org/webapplications-003/).

### What is this repository for? ###

* It contains the latest iteration of my progress as I follow the course
* We are currently onto the third version
* This repository is probably of little or no value to anyone else

### How do I get set up? ###

* Don't bother

### Contribution guidelines ###

* As I am doing this to follow a MOOC, there is absolutely NO reason to contribute
* Rather you can go to the [Web Application Architectures MOOC on Coursera](https://class.coursera.org/webapplications-003/) yourself

### Who do I talk to? ###

* Repo owner or admin
* Noone else